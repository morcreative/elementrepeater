(function($) {

	$.fn.elementRepeater = function(options) {
		var settings = $.extend ({
			url: false,
			elemToCopy: false,
			placement: false
		}, options);

		var self = this;
		var hasErrors = false;

		if (!options.url) {
			console.error('elementRepeater: \"url\" is empty.');
			hasErrors = true;
		}
		if (!options.elemToCopy) {
			console.error('elementRepeater: \"elemToCopy\" is empty.');
			hasErrors = true;
		}
		if (!options.placement) {
			console.error('elementRepeater: \"placement\" is empty.');
			hasErrors = true;
		}
		
		if (!hasErrors) {
			var container = $('<div/>');
            switch(options.placement) {
              case 'append':
                self.append(container.load(options.url + ' ' + options.elemToCopy));
                break;
              case 'before':
                self.before(container.load(options.url + ' ' + options.elemToCopy));
                break;
              case 'after':
                self.after(container.load(options.url + ' ' + options.elemToCopy));
                break;
            }
		}
		return this;
	};
})(jQuery);