# elementRepeater #

***Requires jQuery***

This plugin makes it easy to select an element from a single page and repeat it on any other page.
Programmed with SquareSpace product pages in mind.
 
## Required Options: ##

**url:** the url to the page you want to select the element from  
**elemToCopy:** the element to repeat, select by id '#elementId' or class '.elementClass'   
**placement:** how to attach to the element (append, before, after)  
 
## Example Usage: ##
 

```
#!javascript

$('#elementId').elemRepeater({
	url: 'https://website/pageurl',
	elemToCopy: '#myElementId',
	placement: 'append'
});
```